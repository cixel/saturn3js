/* global THREE, require, THREEx */
var scene,
	camera,
	jupiterLocation = new THREE.Vector3(0, 0, 0),
	sunLocation = new THREE.Vector3(30, 1, 1),
	moons = [];

var tl = new THREE.TextureLoader(THREE.DefaultLoadingManager);

// https://github.com/mrdoob/three.js/blob/master/examples/js/controls/PointerLockControls.js
THREE.PointerLockControls = function(camera) {
	var scope = this;

	camera.rotation.set(0, 0, 0);

	var pitchObject = new THREE.Object3D();
	pitchObject.add(camera);

	var yawObject = new THREE.Object3D();
	yawObject.position.y = 10;
	yawObject.add(pitchObject);

	var PI_2 = Math.PI / 2;

	var onMouseMove = function(event) {
		if (scope.enabled === false) return;

		var movementX = event.movementX || event.mozMovementX || event.webkitMovementX || 0;
		var movementY = event.movementY || event.mozMovementY || event.webkitMovementY || 0;

		yawObject.rotation.y -= movementX * 0.002;
		pitchObject.rotation.x -= movementY * 0.002;

		pitchObject.rotation.x = Math.max(-PI_2, Math.min(PI_2, pitchObject.rotation.x));
	};

	this.dispose = function() {
		document.removeEventListener('mousemove', onMouseMove, false);
	};

	document.addEventListener('mousemove', onMouseMove, false);

	this.enabled = false;

	this.getObject = function() {
		return yawObject;
	};

	this.getDirection = function() {
		// assumes the camera itself is not rotated
		var direction = new THREE.Vector3(0, 0, -1);
		var rotation = new THREE.Euler(0, 0, 0, 'YXZ');

		return function(v) {
			rotation.set(pitchObject.rotation.x, yawObject.rotation.y, 0);
			v.copy(direction).applyEuler(rotation);
			return v;
		};
	}();
};

function sun() {
	var geo = new THREE.SphereGeometry(5, 64, 64);
	tl.load('static/img/sunmap2.jpg', function(texture) {
		var material = new THREE.MeshPhongMaterial({
			map: texture,
			emissive: 0xffffee,
			emissiveIntensity: 1,
			shininess: 60
		});

		var mesh = new THREE.Mesh(geo, material);
		mesh.position = sunLocation;

		var sunlight = new THREE.PointLight('white');
		sunlight.add(mesh);
		sunlight.position = sunLocation;
		scene.add(sunlight);
	});
}

function jupiter() {
	var geometry = new THREE.SphereGeometry(0.5, 64, 64);
	tl.load('static/img/jupiter_texture.jpeg', function(texture) {
		var material = new THREE.MeshPhongMaterial({
			map: texture
		});

		var mesh = new THREE.Mesh(geometry, material);
		mesh.position = jupiterLocation;
		scene.add(mesh);
	});
}

function callisto() {
	var geometry = new THREE.SphereGeometry(0.1, 20, 20);
	tl.load('static/img/callistomap.jpg', function(texture) {
		var material = new THREE.MeshPhongMaterial({
			map: texture,
			bumpMap: texture,
			bumpScale: 0.01
		});

		var mesh = new THREE.Mesh(geometry, material);
		mesh.position = new THREE.Vector3(.5, .9, .2);
		mesh.rotation = new THREE.Vector3(0, 2 * Math.PI / 3 * 2, 0);

		var moon = new THREE.Object3D();
		moon.position = jupiterLocation;
		moon.rotation.z = 0.2;
		moon.add(mesh);
		scene.add(moon);
		moons.push(moon);
	});
}

function io() {
	var geometry = new THREE.SphereGeometry(0.1, 20, 20);
	tl.load('static/img/iomap2.jpg', function(texture) {
		var material = new THREE.MeshPhongMaterial({
			map: texture
		});

		var mesh = new THREE.Mesh(geometry, material);
		mesh.position = new THREE.Vector3(1.1, .5, .3);

		var moon = new THREE.Object3D();
		moon.position = jupiterLocation;
		moon.rotation.y = 0.6;
		moon.rotation.x = 0.8;
		moon.add(mesh);
		scene.add(moon);
		moons.push(moon);
	});
}

function ganymede() {
	var geometry = new THREE.SphereGeometry(0.1, 20, 20);
	tl.load('static/img/ganymedemap.jpg', function(texture) {
		var material = new THREE.MeshPhongMaterial({
			map: texture,
			bumpMap: texture,
			bumpScale: 0.05
		});

		var mesh = new THREE.Mesh(geometry, material);
		mesh.position = new THREE.Vector3(-.7, -.4, -.4);

		var moon = new THREE.Object3D();
		moon.position = jupiterLocation;
		moon.rotation.y = 0.6;
		moon.rotation.x = 0.2;
		moon.add(mesh);
		scene.add(moon);
		moons.push(moon);
	});
}

function europa() {
	var geometry = new THREE.SphereGeometry(0.1, 20, 20);
	tl.load('static/img/europamap.jpg', function(texture) {
		var material = new THREE.MeshPhongMaterial({
			map: texture
		});

		var mesh = new THREE.Mesh(geometry, material);
		mesh.position = new THREE.Vector3(-1.9, -.5, -.9);

		var moon = new THREE.Object3D();
		moon.position = jupiterLocation;
		moon.rotation.x = 0.2;
		moon.add(mesh);
		scene.add(moon);
		moons.push(moon);
	});
}

function starfield() {
	tl.load('static/img/galaxy_starfield.png', function(texture) {

		var material = new THREE.MeshBasicMaterial({
			map: texture,
			side: THREE.BackSide
		});
		var geometry = new THREE.SphereGeometry(400, 32, 32);
		var mesh = new THREE.Mesh(geometry, material);
		scene.add(mesh);
	});
}

function addSceneElements() {
	sun();
	jupiter();
	callisto();
	ganymede();
	io();
	europa();
	starfield();
}

function jupiterMain() {
	var renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);

	scene = new THREE.Scene();
	camera = new THREE.PerspectiveCamera(40, window.innerWidth / window.innerHeight, 0.01, 1000);

	var controls = new THREE.PointerLockControls(camera);
	//camera.lookAt(jupiterLocation);
	//camera.position = jupiterLocation;
	scene.add(controls.getObject());
	controls.enabled = true;

	// declare the rendering loop queue
	var onRenderFcts = [];

	var keyboard = new THREEx.KeyboardState();

	addSceneElements();

	// tick rotation for moons
	onRenderFcts.push(function( /*delta, now*/ ) {
		moons.forEach(function(moon) {
			moon.rotation.z += 0.01;
		});
	});

	// tick movement of camera
	onRenderFcts.push(function(delta /*, now*/ ) {
		var movementSpeed = 5;
		if (keyboard.pressed('down') || keyboard.pressed('s')) {
			controls.getObject().translateZ(movementSpeed * delta);
		} else if (keyboard.pressed('up') || keyboard.pressed('w')) {
			controls.getObject().translateZ(0 - (movementSpeed * delta));
		} else if (keyboard.pressed('left') || keyboard.pressed('a')) {
			controls.getObject().translateX(0 - (movementSpeed * delta));
		} else if (keyboard.pressed('right') || keyboard.pressed('d')) {
			controls.getObject().translateX(movementSpeed * delta);
		} else if (keyboard.pressed('space')) {
			controls.getObject().translateY(movementSpeed * delta);
		} else if (keyboard.pressed('x')) {
			controls.getObject().translateY(0 - (movementSpeed * delta));
		}
	});

	var movementInstructions = document.createElement('div');
	movementInstructions.style.position = 'absolute';
	movementInstructions.style.width = 100;
	movementInstructions.style.height = 100;
	movementInstructions.style.backgroundColor = 'grey';
	movementInstructions.innerHTML = 'W - forward<br> A - left<br> S - back<br> D - right<br> space - up<br> X - down';
	movementInstructions.style.top = 10 + 'px';
	movementInstructions.style.left = 10 + 'px';
	document.body.appendChild(movementInstructions);

	// render the scene
	onRenderFcts.push(function() {
		renderer.render(scene, camera);
	});

	// render loop
	var lastTime = null;
	requestAnimationFrame(function animate(now) {
		// keep looping
		requestAnimationFrame(animate);

		// measure time
		console.log(camera);
		lastTime = lastTime || now - 1000 / 60;
		var delta = Math.min(200, now - lastTime);
		lastTime = now;
		// call each update function
		onRenderFcts.forEach(function(onRenderFct) {
			onRenderFct(delta / 1000, now / 1000);
		});
	});
}
